#!/usr/bin/env bash
# BEGIN SCRIPT HEADER
# MakeSBo - tool that will help you using local SlackBuild scripts
# This program is licensed under 3-clause BSD license
# Creator: glowiak1111@yandex.com
# END SCRIPT HEADER
SELF=makesbo
SELF_PROG=$0
SELF_VER=0.2

BUILD_PROG="$(ls *.SlackBuild | sed 's/.SlackBuild//')"

# now time for flags
for flag in $*
do
	case $flag in
		-i)
			SELF_INSTALL=TRUE
			;;
		-d)
			SELF_REVDEP=TRUE
			;;
		--help)
			echo "$SELF - tool that will help you using local SlackBuild scripts"
			echo "Usage: $SELF {options}"
			echo "Options:"
			echo "-i  | install package after creation"
			echo "-d  | install dependiences before building (requires sbotools)"
			exit 0
			;;
		*)
			;;
	esac
done

msg_error()
{
	echo "ERROR: $*"
	exit 1
}

SBO_DIR=$(pwd) # directory where SlackBuild scripts are located
# fast checks on SlackBuild scripts
if [ ! -f "$SBO_DIR/$BUILD_PROG.SlackBuild" ]; then
	echo "ERROR: Cannot found SlackBuild script in current directory!"
	exit 1
fi
if [ ! -f "$SBO_DIR/$BUILD_PROG.info" ]; then
	echo "ERROR: Cannot found SlackBuild info file in current directory!"
	exit 1
fi
if [ ! -f "$SBO_DIR/slack-desc" ]; then
	echo "ERRPR: Cannot found slack-desc file in current directory!"
	exit 1
fi
. $SBO_DIR/*.info # get information from info file
echo "INFO: Building $PRGNAM v$VERSION"
for file in $DOWNLOAD
do
	echo "Downloading $file..."
	curl -L -O $file
done
if [ -z "$DOWNLOAD_x86_64" ]; then
	for filez in $DOWNLOAD_x86_64
	do
		curl -L -O $filez
	done
fi
if [ ! "$(whoami)" == "root" ]; then
	echo "ERROR: SlackBuilds needs to be runned as root!"
	exit 1
fi
if [ "$SELF_REVDEP" == "TRUE" ]; then
	for depkg in $REQUIRES
	do
		yes | sboinstall $depkg || msg_error "Couldn't run sboinstall!"
	done
fi
sh $PRGNAM.SlackBuild || msg_error "SlackBuild script returned error!"
if [ "$SELF_INSTALL" == "TRUE" ]; then
	installpkg /tmp/$PRGNAM-*.t?z || upgradepkg --reinstall /tmp/$PRGNAM-*.t?z
fi
echo "$SELF completed action: build"
exit 0
