SRCDIR=	./src
SRC=	makesbo
DESTDIR=
PREFIX=	/usr

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp ${SRCDIR}/${SRC}.sh ${DESTDIR}${PREFIX}/bin/${SRC}
	chmod 775 ${DESTDIR}${PREFIX}/bin/${SRC}

uninstall:
	rm -rf ${DESTDIR}${PREFIX}/bin/${SRC}
